

function BliepSpeech() {
  var clientId = "HZYvQd4MSCXvxVlg";
  var loginForm = $("#login-form");;
  var client = new $.BliepClient({ clientId: clientId, authenticate: true, debug: true });;
  var speechEye = $("#speech-eye");
  var transcript = $("#transcript");
  var profile = null;
    
  function init() {
    console.log("BliepSpeech Initialized");

    if (client.isUserAuthenticated()) {
      loginForm.hide();
      onAuthenticated();
    } else {
      speechEye.hide();
      loginForm.on("submit", onLogin);
      speak("Welcome to bliep");
    }
  }

  function onLogin(e) {
    e.preventDefault();

    var email = $("input[name='email']").val();
    var password = $("input[name='password']").val();

    console.log(email, password);

    client.authenticate(email, password).done(function(data) {
      console.log(data);
      
      loginForm.hide();

      speechEye.show();
      onAuthenticated();

    }).fail(function(data) {
        speak("Login failed");// + data.message);
    });
  }

  function onAuthenticated() {
    speak("Loading in progress");
    client.viewProfile().done(function(data) {
      console.log(data);
      profile = data.data;

      speak("Hallo " + profile.name + ".", startListening);
    });
  }

  function startListening() {
    recognition.start();
  }

  var doAfterConfirm = null;
  var confirming = false;

  function confirmBySpeech(f) {
    doAfterConfirm = f;
    confirming = true;
  }

  function onSpokenText(text) {
    console.log(text);
    if (confirming) {
      if (text.indexOf("no") != -1) {
        speak("Roger, no confirmed.", startListening);

      } else if (text.indexOf("yes") != -1) {

        if (doAfterConfirm) {
          doAfterConfirm();
          return;
        }
      }
    }

    if(text.indexOf("credit") != -1) {
        speak("Your credit is " + profile.balance + " euro and "
          + profile.calltime.minutes + " minutes and " +profile.calltime.seconds + " seconds", startListening);
      return;
    }

    if (text.indexOf("status") != -1) {
      speak("Your current tariff is " + profile.state, startListening);
      return;
    }

    if (text.indexOf("switch off") != -1) {
      client.updateSIMCardState(false, false, false).done(function () {
        speak("Bliep is turned off", startListening);
      }).fail(function(e) {
        speak("Could not turn bliep off: " + e.message, startListening);
      })
      speak("Turning off bliep");
      return;
    }


    if (text.indexOf("switch on") != -1) {
      client.updateSIMCardState(true, false, false).done(function () {
        speak("Bliep is turned on", startListening);
      }).fail(function(e) {
        speak("Could not turn bliep on: " + e.message, startListening);
      })
      speak("Turning on bliep");
      return;
    }

    speak("Unknown command", startListening);
  }


  var SpeechRecognition = window.SpeechRecognition || 
                        window.webkitSpeechRecognition || 
                        window.mozSpeechRecognition || 
                        window.oSpeechRecognition || 
                        window.msSpeechRecognition

  if (!SpeechRecognition) {
    alert("No speech recognization supported in this browser");
  }
  recognition = new SpeechRecognition();
  recognition.lang = "en";
  recognition.continuous = true;
  recognition.interimResults = false;

  recognition.onstart = function() {
    console.log("recognition.onstart");
    speechEye.addClass("listening");

  }
  recognition.onresult = function(event) {

    console.log("recognition.onresult", event);
    var interim_transcript = '';
    var final_transcript = '';

    for (var i = event.resultIndex; i < event.results.length; ++i) {
      if (event.results[i].isFinal) {
        final_transcript += event.results[i][0].transcript;
      } else {
        interim_transcript += event.results[i][0].transcript;
      }
    }
    transcript.text(final_transcript);
    recognition.stop();
    onSpokenText(final_transcript);
  }
  recognition.onerror = function(event) {

    console.log("recognition.onerror", event);
    speechEye.addClass("error");
  }
  recognition.onend = function(e) {
    console.log("recognition.onend", e);
  }

  function speak(text, cb) {
    transcript.text(text);

    recognition.stop();

    speechEye.removeClass("listening");
    speechEye.addClass("speaking");

    var url = "http://translate.google.com/translate_tts?tl=en&q=" + encodeURIComponent(text);

    var fullCb = function() {
      console.log("speechSynthesis.onend called");
      speechEye.removeClass("speaking");
      transcript.text("");
      if (cb) {
        cb();
      }
    };

    /*
    var audio = $("#audio");
    audio.off("ended");
    audio.off("error");
    audio.on("ended", fullCb);
    audio.on("error", fullCb);

    audio.attr("src", url);
    chrome.tts.speak(text, {'lang': 'en-US', 'rate': 1.0}, fullCb);*/
    

    var utterance = new SpeechSynthesisUtterance(text);
    $(utterance).on("end", fullCb);
    speechSynthesis.speak(utterance);
  }

  init();

  return recognition;
}

$(function() {
  window.bliepSpeech = BliepSpeech();
});